/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tree;

import java.io.Serializable;

/**
 * @author Дмитрий
 */
public class Node implements Serializable {
    private int frequency;
    private String characters;
    private Node left;
    private Node right;
    private Node prev;

    public Node(int frequency, String characters) {
        this.setFrequency(frequency);
        this.setCharacters(characters);
        setLeft(null);
        setRight(null);
        setPrev(null);
    }

    public Node() {

    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getCharacters() {
        return characters;
    }

    public void setCharacters(String characters) {
        this.characters = characters;
    }

    public Node getLeft() {
        return left;
    }

    public void setLeft(Node left) {
        this.left = left;
    }

    public Node getRight() {
        return right;
    }

    public void setRight(Node right) {
        this.right = right;
    }

    public Node getPrev() {
        return prev;
    }

    public void setPrev(Node prev) {
        this.prev = prev;
    }
}
