/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tree;

import java.io.Serializable;
import java.util.TreeMap;

/**
 * @author Дмитрий
 */
public class Tree implements Serializable {
    private Node root;
    private int numberOfBits;

    public Tree() {

    }

    public Node merge(Node left, Node right) {
        Node root = new Node(left.getFrequency() + right.getFrequency(), left.getCharacters() + right.getCharacters());
        left.setPrev(root);
        right.setPrev(root);
        root.setLeft(left);
        root.setRight(right);
        this.setRoot(root);
        return root;
    }

    private void encodingHelper(Node node, StringBuffer stringBuffer, TreeMap<Character, String> treeMap) {
        if (node != null) {
            if (node.getLeft() != null) {
                stringBuffer.append('0');
                encodingHelper(node.getLeft(), stringBuffer, treeMap);
                stringBuffer.delete(stringBuffer.length() - 1, stringBuffer.length());
            }
            if (node.getRight() != null) {
                stringBuffer.append('1');
                encodingHelper(node.getRight(), stringBuffer, treeMap);
                stringBuffer.delete(stringBuffer.length() - 1, stringBuffer.length());
            } else {
                treeMap.put(node.getCharacters().toCharArray()[0], stringBuffer.toString());
            }
        }
    }

    public TreeMap<Character, String> encoding() {
        StringBuffer stringBuffer = new StringBuffer();
        TreeMap<Character, String> treeMap = new TreeMap<>();
        this.encodingHelper(getRoot(), stringBuffer, treeMap);
        return treeMap;
    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }

    public int getNumberOfBits() {
        return numberOfBits;
    }

    public void setNumberOfBits(int numberOfBits) {
        this.numberOfBits = numberOfBits;
    }
}
