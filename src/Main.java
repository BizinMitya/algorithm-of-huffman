import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Main {

//    Война и Мир 1-2 том, 2.5 МБ
//    Выполняется сжатие...
//    Готово! Сжатие заняло 00:03:910
//    Выполняется распаковка...
//    Готово! Распаковка заняла 15:45:337

//    Евгений Онегин, 276 КБ
//    Выполняется сжатие...
//    Готово! Сжатие заняло 00:00:960
//    Выполняется распаковка...
//    Готово! Распаковка заняла 00:09:565

    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        SimpleDateFormat formatDate = new SimpleDateFormat("mm:ss:S");
        long time;
        System.out.println("Выполняется сжатие...");
        time = System.currentTimeMillis();
        Algorithm.packing();
        time = System.currentTimeMillis() - time;
        calendar.setTimeInMillis(time);
        System.out.println("Готово! Сжатие заняло " + formatDate.format(calendar.getTime()));
        System.out.println("Выполняется распаковка...");
        time = System.currentTimeMillis();
        Algorithm.unpacking();
        time = System.currentTimeMillis() - time;
        calendar.setTimeInMillis(time);
        System.out.println("Готово! Распаковка заняла " + formatDate.format(calendar.getTime()));
    }
}