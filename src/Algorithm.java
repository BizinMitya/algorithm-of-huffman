import sun.security.util.BitArray;
import tree.Node;
import tree.Tree;

import java.io.*;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.TreeMap;

/**
 * Created by Dmitriy on 15.05.2016.
 */
public abstract class Algorithm {

    public static void packing() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("text.txt"));
             FileOutputStream fileOutputStream = new FileOutputStream("packingText.bin");
             ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("tree.bin"))) {
            String string = "";
            StringBuffer text = new StringBuffer();
            while ((string = bufferedReader.readLine()) != null) {
                text.append(string).append("\n");
            }
            string = text.toString();
            Tree tree = new Tree();
            List<Node> list = new ArrayList<>();
            int min1, min2;
            char c;
            Node left = null;
            Node right = null;
            boolean flag = false;
            for (int i = 0; i < string.length(); i++) {
                flag = false;
                c = string.charAt(i);
                for (Node node : list) {
                    if (node.getCharacters().toCharArray()[0] == c) {
                        node.setFrequency(node.getFrequency() + 1);
                        flag = true;
                        break;
                    }
                }
                if (!flag) {
                    list.add(new Node(1, Character.toString(c)));
                }
            }
            while (list.size() != 1) {
                min1 = Integer.MAX_VALUE;
                min2 = Integer.MAX_VALUE;
                for (Node node : list) {
                    if (node.getFrequency() < min1) {
                        min1 = node.getFrequency();
                        left = node;
                    }
                }
                list.remove(left);
                for (Node node : list) {
                    if (node.getFrequency() < min2) {
                        min2 = node.getFrequency();
                        right = node;
                    }
                }
                list.remove(right);
                list.add(tree.merge(left, right));
            }
            TreeMap<Character, String> treeMap = tree.encoding();
            StringBuffer resultString = new StringBuffer();
            for (int i = 0; i < string.length(); i++) {
                resultString.append(treeMap.get(string.charAt(i)));
            }
            BitArray bitArray = null;
            if (resultString.length() % 8 != 0) {
                bitArray = new BitArray(resultString.length() + (8 - resultString.length() % 8));
            } else {
                bitArray = new BitArray(resultString.length());
            }
            for (int i = 0; i < resultString.length(); i++) {
                if (resultString.charAt(i) == '0') {
                    bitArray.set(i, false);
                }
                if (resultString.charAt(i) == '1') {
                    bitArray.set(i, true);
                }
            }
            if (resultString.length() % 8 != 0) {
                for (int i = 0; i < 8 - resultString.length() % 8; i++) {
                    bitArray.set(resultString.length() + i, false);
                }
            }
            tree.setNumberOfBits(resultString.length() % 8);
            byte[] result = bitArray.toByteArray();
            fileOutputStream.write(result);
            objectOutputStream.writeObject(tree);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void unpacking() {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("unpackingText.txt"));
             FileInputStream fileInputStream = new FileInputStream("packingText.bin");
             ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("tree.bin"))) {
            Tree tree = (Tree) objectInputStream.readObject();
            int numberOfBits = tree.getNumberOfBits();
            List<Byte> list = new LinkedList<>();
            byte[] byteFromFile = new byte[1];
            while (fileInputStream.read(byteFromFile) != -1) {
                list.add(byteFromFile[0]);
            }
            byte a;
            boolean arr[] = new boolean[8];
            BitArray bitArray = null;
            if (numberOfBits != 0) {
                bitArray = new BitArray((list.size() - 1) * 8 + numberOfBits);
            } else {
                bitArray = new BitArray(list.size() * 8);
            }
            int k = 0;
            for (int i = 0; i < list.size(); i++) {
                a = list.get(i);
                if (i != list.size() - 1 || (i == list.size() - 1 && numberOfBits == 0)) {
                    for (int j = 0; j < 8; j++) {
                        if ((a & 0x1) == 1) {
                            arr[j] = true;
                        }
                        if ((a & 0x1) == 0) {
                            arr[j] = false;
                        }
                        a >>= 1;
                    }
                    for (int j = 7; j >= 0; j--) {
                        if (arr[j]) {
                            bitArray.set(k, true);
                        }
                        if (!arr[j]) {
                            bitArray.set(k, false);
                        }
                        k++;
                    }
                } else {
                    for (int j = 0; j < 8; j++) {
                        if ((a & 0x1) == 1) {
                            arr[j] = true;
                        }
                        if ((a & 0x1) == 0) {
                            arr[j] = false;
                        }
                        a >>= 1;
                    }
                    for (int j = 7; j > 7 - numberOfBits; j--) {
                        if (arr[j]) {
                            bitArray.set(k, true);
                        }
                        if (!arr[j]) {
                            bitArray.set(k, false);
                        }
                        k++;
                    }
                }
            }
            Node node = tree.getRoot();
            StringBuffer stringBuffer = new StringBuffer();
            for (int i = 0; i < bitArray.length(); i++) {
                if (!bitArray.get(i)) {
                    if (node.getLeft() != null) {
                        node = node.getLeft();
                    } else {
                        stringBuffer.append(node.getCharacters());
                        node = tree.getRoot().getLeft();
                    }
                }
                if (bitArray.get(i)) {
                    if (node.getRight() != null) {
                        node = node.getRight();
                    } else {
                        stringBuffer.append(node.getCharacters());
                        node = tree.getRoot().getRight();
                    }
                }
            }
            stringBuffer.append(node.getCharacters());
            bufferedWriter.write(stringBuffer.toString());
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
