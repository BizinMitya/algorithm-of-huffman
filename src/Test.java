import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dmitriy on 16.05.2016.
 */
public class Test {
    public static void main(String[] args) {
        try (FileOutputStream fileOutputStream = new FileOutputStream("test.bin");
             FileInputStream fileInputStream = new FileInputStream("test.bin")) {
            fileOutputStream.write(34);
            fileOutputStream.write(12);
            fileOutputStream.write(16);
            fileOutputStream.write(76);
            fileOutputStream.close();
            boolean arr[] = new boolean[8];
            byte res[] = new byte[1];
            byte a;
            List<Byte> list = new ArrayList<>(5);
            while ((fileInputStream.read(res)) != -1) {
                list.add(res[0]);
            }
            for (int i = 0; i < list.size(); i++) {
                a = list.get(i);
                for (int j = 0; j < 8; j++) {
                    if ((a & 0x1) == 1) {
                        arr[j] = true;
                    }
                    if ((a & 0x1) == 0) {
                        arr[j] = false;
                    }
                    a >>= 1;
                }
                for (int j = 7; j >= 0; j--) {
                    if (arr[j]) {
                        System.out.print(1);
                    }
                    if (!arr[j]) {
                        System.out.print(0);
                    }
                }
                System.out.println();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
